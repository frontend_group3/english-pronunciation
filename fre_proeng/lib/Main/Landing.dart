// ignore: file_names
import 'package:flutter/material.dart';
import '../Sing-in/SignIn.dart';
import '../Sing-in/signUp.dart';

class LandingPage extends StatelessWidget {
  const LandingPage({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: const Text('Landing'),
        centerTitle: true,
      ),
      body: Column(children: [
        ElevatedButton(
            child: const Text('Signin'),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const SigninPage()));
            }),
        ElevatedButton(
            child: const Text('Signup'),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const SignupPage()));
            }),
      ]));
}
