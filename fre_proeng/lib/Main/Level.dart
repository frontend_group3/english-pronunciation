import 'package:flutter/material.dart';
import 'list.dart';
import 'Category.dart';
import 'Account.dart';
import 'Landing.dart';

class LevelPage extends StatelessWidget {
  const LevelPage({super.key});

  get child => null;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 255, 255, 255),
          title: const Text(
            'Level',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
        ),
        backgroundColor: const Color.fromRGBO(228, 217, 255, 1),
        // body: Center(
        //   child: ElevatedButton(
        //     style: ElevatedButton.styleFrom(
        //       padding: const EdgeInsets.all(20),
        //     ),
        //     child: const Text('NextScreen', style: TextStyle(fontSize: 20)),
        //     onPressed: () {
        //       Navigator.push(
        //         context,
        //         MaterialPageRoute(builder: (context) => const CategoryPage()),
        body: Column(
          children: [
            Row(
              children: [
                ElevatedButton(
                    child: const Text('Level1'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const CategoryPage()));
                    }),
                ElevatedButton(
                    child: const Text('Level2'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const CategoryPage()));
                    }),
                ElevatedButton(
                    child: const Text('Level3'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const CategoryPage()));
                    }),
                ElevatedButton(
                    child: const Text('List'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ListPage()));
                    }),
                ElevatedButton(
                    child: const Text('My Account'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const AccountPage()));
                    }),
                ElevatedButton(
                    child: const Text('Landing'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LandingPage()));
                    }),
              ],
            ),
            GridView.count(
              physics: const NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              primary: true,
              padding: const EdgeInsets.all(20),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 6,
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.all(8),
                    color: Colors.white,
                    child: const Text('0%')),
                Container(
                    padding: const EdgeInsets.all(8),
                    color: Colors.black,
                    child: const Text('0%')),
                Container(
                    padding: const EdgeInsets.all(8),
                    color: Colors.white,
                    child: const Text('0%')),
              ],
            ),
          ],
        ),

        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.menu_book),
              label: 'Level',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.edit),
              label: 'List',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: 'Account',
            ),
          ],
          selectedItemColor: const Color.fromRGBO(185, 148, 231, 1),
        ),
      );
}
