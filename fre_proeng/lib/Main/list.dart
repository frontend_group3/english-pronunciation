import 'package:flutter/material.dart';
import 'package:fre_proeng/Child_widgets/List/sound_list.dart';
import 'package:fre_proeng/Child_widgets/List/word_list.dart';

class ListPage extends StatefulWidget {
  const ListPage({super.key});

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  var currentPage = 'sound list';

  void _changePage() {
    setState(() {
      if (currentPage == 'sound list') {
        currentPage = 'word list';
      } else {
        currentPage = 'sound list';
      }
    });
    print(currentPage);
  }
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: const Text('List'),
        centerTitle: true,
      ),
      backgroundColor: const Color.fromRGBO(254, 215, 215, 1),
      body: SingleChildScrollView(
          child: Column(children: [
        Container(
          padding: const EdgeInsets.only(top: 40, left: 30),
          child: Column(
            children: [
              Row(children: [
                Text(
                  "My $currentPage",
                  style: const TextStyle(
                      fontSize: 25,
                      color: Color.fromRGBO(44, 68, 167, 1),
                      fontWeight: FontWeight.w600),
                )
              ])
            ],
          ),
        ),
        const SizedBox(
          height: 25,
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(30),
                topLeft: Radius.circular(30),
              ),
            ),
            child: (currentPage == 'sound list')
                ? SoundListPage(_changePage)
                : WordListPage(_changePage))
      ])));
}
