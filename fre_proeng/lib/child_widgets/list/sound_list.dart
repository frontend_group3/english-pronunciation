import 'package:flutter/material.dart';
import 'package:fre_proeng/child_widgets/list/word_list.dart';

const List<String> categories = <String>[
  'Consonants',
  'Vowels',
  'Clusters',
  'Word Ending'
];

final List soundList = [
  '/A/',
  '/B/',
  '/C/',
  '/D/',
  '/E/',
  '/F/',
  '/G/',
  '/H/',
  '/I/',
  '/J/',
  '/K/',
  '/L/',
  '/M/',
  '/N/',
  '/O/',
  '/P/',
  '/Q/',
  '/R/',
  '/S/',
  '/T/',
  '/U/',
  '/V/',
  '/W/',
  '/X/',
  '/Y/',
  '/Z/'
];

class SoundListPage extends StatefulWidget {
  final Function _changePage;
  const SoundListPage(this._changePage, {super.key});

  @override
  State<SoundListPage> createState() => _SoundListPageState();
}

class _SoundListPageState extends State<SoundListPage> {
  String dropdownValue = categories.first;
  final searchedWord = TextEditingController();
  List showedWords = List.from(soundList);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        padding: const EdgeInsets.only(top: 10, left: 30, right: 30),
        child: Column(
          children: [
            const SizedBox(
              height: 15,
            ),
            Column(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 100,
                      height: 30,
                      child: ElevatedButton(
                        child: const Text('WordList'),
                        onPressed: () {
                          widget._changePage();
                        },
                      ),
                    ),
                    const Spacer(),
                    Container(
                      alignment: Alignment.center,
                      height: 40,
                      width: 120,
                      decoration: const ShapeDecoration(
                        color: Color.fromRGBO(44, 68, 167, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                      ),
                      child: DropdownButton<String>(
                        dropdownColor: const Color.fromRGBO(44, 68, 167, 1),
                        underline: const SizedBox(),
                        value: dropdownValue,
                        icon: const Icon(
                          Icons.arrow_drop_down_rounded,
                          color: Colors.white,
                        ),
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            dropdownValue = value!;
                          });
                        },
                        items: categories
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                TextField(
                  decoration: const InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 10.0),
                      prefixIcon: Icon(Icons.search),
                      labelText: 'Search',
                      labelStyle:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                      filled: true,
                      fillColor: Color.fromRGBO(228, 217, 255, 0.5),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          borderSide: BorderSide.none)),
                  controller: searchedWord,
                  onChanged: (_) {
                    setState(() {
                      showedWords = List.from(soundList);
                      showedWords = showedWords
                          .where((element) => element
                              .toString()
                              .toUpperCase()
                              .contains(searchedWord.text.toUpperCase()))
                          .toList();
                    });
                    print(showedWords);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
      const SizedBox(
        height: 15,
      ),
      Container(
        padding: const EdgeInsets.only(right: 30, left: 30),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 100,
              mainAxisSpacing: 12,
              crossAxisSpacing: 12,
            ),
            scrollDirection: Axis.vertical,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: showedWords.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Color.fromRGBO(228, 217, 255, 100),
                ),
                child: Center(
                  child: Text(
                    showedWords[index],
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            }),
      ),
    ]);
  }
}