import 'package:flutter/material.dart';

// ignore: must_be_immutable
class WordListPage extends StatefulWidget {
  final Function _changePage;
  const WordListPage(this._changePage, {super.key});

  @override
  State<WordListPage> createState() => _WordListPageState();
}

final List wordList = [
  'apple',
  'banana',
  'cherry',
  'date',
  'elderberry',
  'fig',
  'grape',
  'honeydew',
  'kiwi',
  'lemon',
  'mango',
  'nectarine',
  'orange',
  'peach',
  'quince',
  'raspberry',
  'strawberry',
  'tangerine',
  'ugli',
  'watermelon',
  'xigua',
  'yellow',
  'zucchini',
  'apricot',
  'blueberry',
  'cantaloupe',
  'damson',
  'eagle',
  'fig',
  'gooseberry'
];

class _WordListPageState extends State<WordListPage> {
  String dropdownValue = 'A to Z';
  final searchedWord = TextEditingController();

  List showedWords = List.from(wordList);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        padding: const EdgeInsets.only(top: 10, left: 30, right: 30),
        child: Column(
          children: [
            const SizedBox(
              height: 15,
            ),
            Column(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 100,
                      height: 30,
                      child: ElevatedButton(
                        child: const Text('SoundList'),
                        onPressed: () {
                          widget._changePage();
                        },
                      ),
                    ),
                    const Spacer(),
                    DropdownButton<String>(
                      value: dropdownValue,
                      icon: const Icon(Icons.arrow_drop_down_rounded),
                      elevation: 16,
                      style: const TextStyle(color: Colors.deepPurple),
                      underline: Container(
                        height: 2,
                        color: Colors.deepPurpleAccent,
                      ),
                      onChanged: (String? value) {
                        // This is called when the user selects an item.
                        setState(() {
                          dropdownValue = value!;
                          if (dropdownValue == 'A to Z') {
                            showedWords.sort((a, b) => a
                                .toString()
                                .toLowerCase()
                                .compareTo(b.toString().toLowerCase()));
                          }
                          if (dropdownValue == 'Z to A') {
                            showedWords.sort((a, b) => b
                                .toString()
                                .toLowerCase()
                                .compareTo(a.toString().toLowerCase()));
                          }
                        });
                      },
                      items: <String>[
                        'A to Z',
                        'Z to A',
                        'Level',
                        'Category',
                        'Date last learned'
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    )
                  ],
                ),
                TextField(
                  decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.search), labelText: 'Search'),
                  controller: searchedWord,
                  onChanged: (_) {
                    setState(() {
                      showedWords = List.from(wordList);
                      showedWords = showedWords
                          .where((element) =>
                              element.toString().contains(searchedWord.text))
                          .toList();
                    });
                    print(showedWords);
                  },
                  cursorColor: Colors.grey,
                )
              ],
            ),
          ],
        ),
      ),
      const SizedBox(
        height: 15,
      ),
      Container(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: const EdgeInsets.all(8),
            itemCount: showedWords.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(left: 15, right: 15),
                margin: const EdgeInsets.only(bottom: 5),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Color.fromRGBO(228, 217, 255, 100),
                ),
                height: 40,
                child: Row(
                  children: [
                    Text(
                      showedWords[index],
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Spacer(),
                    const Icon(
                      Icons.volume_up,
                      color: Colors.black,
                    )
                  ],
                ),
              );
            }),
      )
    ]);
  }
}
